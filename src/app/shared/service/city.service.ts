import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {City} from '../model/city';
import {Country} from '../model/country';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http: HttpClient) {
  }

  getAllCities(): Observable<City[]> {
    return this.http.get<City[]>('http://localhost:8086/rest/city/all');
  }

  getCity(id: number): Observable<City> {
    return this.http.get<City>('http://localhost:8086/rest/city/get/' + id);
  }

  saveCity(city: City): void {
    this.http.post('http://localhost:8086/rest/city/save', city)
      .subscribe(value => {
        console.log(value);
      });
  }

  updateCity(city: City): void {
    this.http.put('http://localhost:8086/rest/city/update', city)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteCity(id: number): Observable<City> {
    return this.http.delete<City>('http://localhost:8086/rest/city/delete/' + id);
  }
}

