import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Airport} from '../model/Airport';
import {City} from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) {
  }

  getAllAirport(): Observable<Airport[]> {
    return this.http.get<Airport[]>('http://localhost:8086/rest/airport/all');
  }

  getAirport(id: number): Observable<Airport> {
    return this.http.get<Airport>('http://localhost:8086/rest/airport/get/' + id);
  }

  saveAirport(airport: Airport): void {
    this.http.post('http://localhost:8086/rest/airport/save', airport)
      .subscribe(value => {
        console.log(value);
      });
  }
  updateAirport(airport: Airport): void {
    this.http.put('http://localhost:8086/rest/airport/update', airport)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteAirport(id: number): Observable<Airport> {
    return this.http.delete<Airport>('http://localhost:8086/rest/airport/delete/' + id);
  }
}
