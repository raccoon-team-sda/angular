import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) {
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>('http://localhost:8086/rest/user/all');
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>('http://localhost:8086/rest/user/get/' + id);
  }

  save(user: User): void {
    this.http.post('http://localhost:8086/rest/user/save', user)
      .subscribe(value => {
      console.log(value);
    });
  }

  update(user: User): void {
    this.http.put('http://localhost:8086/rest/user/update', user)
      .subscribe(value => {
      console.log(value);
    });
  }

  deleteUser(id: number): Observable<User> {
    return this.http.delete<User>('http://localhost:8086/rest/user/delete/' + id);
  }
}
