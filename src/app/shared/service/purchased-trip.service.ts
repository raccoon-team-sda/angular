import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PurchasedTrip} from '../model/purchasedTrip';

@Injectable({
  providedIn: 'root'
})
export class PurchasedTripService {

  constructor(private http: HttpClient) { }

  getAllPurchasedTrip(): Observable<PurchasedTrip[]> {
    return this.http.get<PurchasedTrip[]>('http://localhost:8086/rest/purchasedTrip/all');
  }

  getPurchasedTrip(id: number): Observable<PurchasedTrip> {
    return this.http.get<PurchasedTrip>('http://localhost:8086/rest/purchasedTrip/get/' + id);
  }

  savePurchasedTrip(purchasedTrip: PurchasedTrip): void {
    this.http.post('http://localhost:8086/rest/purchasedTrip/save', purchasedTrip)
      .subscribe(value => {
        console.log(value);
      });
  }
  updatePurchasedTrip(purchasedTrip: PurchasedTrip): void {
    this.http.put('http://localhost:8086/rest/purchasedTrip/update', purchasedTrip)
      .subscribe(value => {
        console.log(value);
      });
  }

  deletePurchasedTrip(id: number): Observable<PurchasedTrip> {
    return this.http.delete<PurchasedTrip>('http://localhost:8086/rest/purchasedTrip/delete/' + id);
  }
}
