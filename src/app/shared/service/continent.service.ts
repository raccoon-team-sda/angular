import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Continent} from '../model/continent';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContinentService {

  constructor(private http: HttpClient) {
  }

  getAllContinents(): Observable<Continent[]> {
    return this.http.get<Continent[]>('http://localhost:8086/rest/continent/all');
  }

  getContinent(id: number): Observable<Continent> {
    return this.http.get<Continent>('http://localhost:8086/rest/continent/get/' + id);
  }

  // getContinentName(name: string): Observable<Continent> {
  //   return this.http.get<Continent>('http://localhost:8086/rest/continent/findByName' + name);
  // }


  saveContinent(continent: Continent): void {
    this.http.post('http://localhost:8086/rest/continent/save', continent)
      .subscribe(value => {
        console.log(value);
      });
  }

  updateContinent(continent: Continent): void {
    this.http.put('http://localhost:8086/rest/continent/update', continent)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteContinent(id: number): Observable<Continent> {
    return this.http.delete<Continent>('http://localhost:8086/rest/continent/delete/' + id);
  }

}
