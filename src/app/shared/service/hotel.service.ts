import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hotel} from '../model/hotel';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) { }

  getAllHotel(): Observable<Hotel[]> {
    return this.http.get<Hotel[]>('http://localhost:8086/rest/hotel/all');
  }

  getHotel(id: number): Observable<Hotel> {
    return this.http.get<Hotel>('http://localhost:8086/rest/hotel/get/' + id);
  }

  saveHotel(hotel: Hotel): void {
    this.http.post('http://localhost:8086/rest/hotel/save', hotel)
      .subscribe(value => {
        console.log(value);
      });
  }
  updateHotel(hotel: Hotel): void {
    this.http.put('http://localhost:8086/rest/hotel/update', hotel)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteHotel(id: number): Observable<Hotel> {
    return this.http.delete<Hotel>('http://localhost:8086/rest/hotel/delete/' + id);
  }
}
