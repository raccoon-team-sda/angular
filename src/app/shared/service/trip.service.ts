import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Trip} from '../model/trip';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http: HttpClient) { }

  getAllTrip(): Observable<Trip[]> {
    return this.http.get<Trip[]>('http://localhost:8086/rest/trip/all');
  }

  getTrip(id: number): Observable<Trip> {
    return this.http.get<Trip>('http://localhost:8086/rest/trip/get/' + id);
  }

  saveTrip(trip: Trip): void {
    this.http.post('http://localhost:8086/rest/trip/save', trip)
      .subscribe(value => {
        console.log(value);
      });
  }
  updateTrip(trip: Trip): void {
    this.http.put('http://localhost:8086/rest/trip/update', trip)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteTrip(id: number): Observable<Trip> {
    return this.http.delete<Trip>('http://localhost:8086/rest/trip/delete/' + id);
  }
}
