import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Country} from '../model/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) {
  }

  getAllCountries(): Observable<Country[]> {
    return this.http.get<Country[]>('http://localhost:8086/rest/country/all');
  }

  getCountry(id: number): Observable<Country> {
    return this.http.get<Country>('http://localhost:8086/rest/country/get/' + id);
  }

  saveCountry(country: Country): void {
    this.http.post('http://localhost:8086/rest/country/save', country)
      .subscribe(value => {
        console.log(value);
      });
  }

  updateCountry(country: Country): void {
    this.http.put('http://localhost:8086/rest/country/update', country)
      .subscribe(value => {
        console.log(value);
      });
  }

  deleteCountry(id: number): Observable<Country> {
    return this.http.delete<Country>('http://localhost:8086/rest/country/delete/' + id);
  }
}
