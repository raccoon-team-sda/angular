export class User {
  id: number;
  name: string;
  lastName: string;
  bornYear: number;
  phoneNumber: number;
}
