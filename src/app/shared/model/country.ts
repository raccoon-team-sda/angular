
export class Country {
  id: number;
  name: string;
  continentId: number;
}
