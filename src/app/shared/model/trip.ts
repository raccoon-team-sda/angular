export class Trip {
  id: number;
  airportFrom: number;
  airportTo: number;
  hotelTo: number;
  cityTo: number;
  departureDate: number;
  returnDate: number;
  countOfDays: number;
  type: string;
  priceForAdult: number;
  priceForChild;
  promotion: string;
  countOfPerson: number;
  description: string;
}
