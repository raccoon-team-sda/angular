import {Component, OnInit} from '@angular/core';
import {AirportService} from '../shared/service/airport.service';
import {Airport} from '../shared/model/Airport';

@Component({
  selector: 'app-airport-list',
  templateUrl: './airport-list.component.html',
  styleUrls: ['./airport-list.component.css']
})
export class AirportListComponent implements OnInit {

  constructor(private airportService: AirportService) {
  }

  airports: Airport[];

  ngOnInit() {
    this.airportService.getAllAirport().subscribe(value => {
      console.log(value);
      this.airports = value;
    });
  }

  delete(id: number): void {
    this.airportService.deleteAirport(id).subscribe(value => {
      this.airports = this.airports.filter(eachAirports => eachAirports.id !== id);
    });
  }
}
