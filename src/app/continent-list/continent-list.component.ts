import {Component, OnInit} from '@angular/core';
import {Continent} from '../shared/model/continent';
import {ContinentService} from '../shared/service/continent.service';

@Component({
  selector: 'app-continent-list',
  templateUrl: './continent-list.component.html',
  styleUrls: ['./continent-list.component.css']
})
export class ContinentListComponent implements OnInit {

  constructor(private continentService: ContinentService) {
  }

  continents: Continent[];

  ngOnInit() {
    this.continentService.getAllContinents().subscribe(value => {
      console.log(value);
      this.continents = value;
    });
  }

  delete(id: number): void {
    this.continentService.deleteContinent(id).subscribe(value => {
      this.continents = this.continents.filter(eachUsers => eachUsers.id !== id);
    });
  }
}
