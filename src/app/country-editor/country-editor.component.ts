import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CountryService} from '../shared/service/country.service';
import {Country} from '../shared/model/country';
import {Continent} from '../shared/model/continent';
import {ContinentService} from '../shared/service/continent.service';

@Component({
  selector: 'app-country-editor',
  templateUrl: './country-editor.component.html',
  styleUrls: ['./country-editor.component.css']
})
export class CountryEditorComponent implements OnInit {

  country: Country = new Country();
  continent: Continent = null;
  continents: Continent[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private countryService: CountryService,
              private continentService: ContinentService) {
  }

  ngOnInit() {
    this.continent = new Continent();
    this.continentService.getAllContinents().subscribe(value => {
      console.log(value);
      this.continents = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    // konwersja typu string do int
    const id: number = parseInt(idParam, 10);
    // wyświetlenie id w przeglądrce w trybie develop(F12)
    console.log('id:' + id);
    // jesli id nie jets nullem i nie jest wartością pustą
    if (id) {
      // wywołanie usługi pobierania użytkownika po id
      this.countryService.getCountry(id).subscribe(value => {
        console.log(value);
        this.country = value;
        this.country.id = id;
      });
    }
  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.country);
    this.country.id = id;
    this.countryService.updateCountry(this.country);
    window.location.href = '/country-list';
  }

  goBack(): void {
    // wywołanie poprzedniego url
    this.location.back();
  }
}
