import {Component, OnInit} from '@angular/core';
import {Airport} from '../shared/model/Airport';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {AirportService} from '../shared/service/airport.service';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';

@Component({
  selector: 'app-airport-editor',
  templateUrl: './airport-editor.component.html',
  styleUrls: ['./airport-editor.component.css']
})
export class AirportEditorComponent implements OnInit {

  airport: Airport = new Airport();
  city: City = null;
  cities: City[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private airportService: AirportService,
              private cityService: CityService) {
  }

  ngOnInit() {
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.airportService.getAirport(id).subscribe(value => {
        console.log(value);
        this.airport = value;
        this.airport.id = id;
      });
    }
  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.airport);
    this.airport.id = id;
    this.airportService.updateAirport(this.airport);
    window.location.href = '/airport-list';
  }

  goBack(): void {
    this.location.back();
  }

}
