import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {HotelService} from '../shared/service/hotel.service';
import {TripService} from '../shared/service/trip.service';
import {Trip} from '../shared/model/trip';
import {City} from '../shared/model/city';
import {Airport} from '../shared/model/Airport';
import {Hotel} from '../shared/model/hotel';
import {AirportService} from '../shared/service/airport.service';

@Component({
  selector: 'app-trip-editor',
  templateUrl: './trip-editor.component.html',
  styleUrls: ['./trip-editor.component.css']
})
export class TripEditorComponent implements OnInit {

  trip: Trip = new Trip();
  city: City = null;
  cities: City[];
  airport: Airport = null;
  airports: Airport[];
  hotel: Hotel = null;
  hotels: Hotel[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private hotelService: HotelService,
              private airportService: AirportService,
              private tripService: TripService) {
  }

  ngOnInit() {
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
    this.airport = new Airport();
    this.airportService.getAllAirport().subscribe(value => {
      console.log(value);
      this.airports = value;
    });
    this.hotel = new Hotel();
    this.hotelService.getAllHotel().subscribe(value => {
      console.log(value);
      this.hotels = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.tripService.getTrip(id).subscribe(value => {
        console.log(value);
        this.trip = value;
        this.trip.id = id;
      });
    }

  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.trip);
    this.trip.id = id;
    this.tripService.updateTrip(this.trip);
    window.location.href = '/trip-list';

  }

  goBack(): void {
    this.location.back();
  }
}
