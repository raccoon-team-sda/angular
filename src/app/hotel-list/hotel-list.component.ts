import {Component, OnInit} from '@angular/core';
import {HotelService} from '../shared/service/hotel.service';
import {Hotel} from '../shared/model/hotel';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {

  constructor(private hotelService: HotelService) {
  }

  hotels: Hotel[];

  ngOnInit() {
    this.hotelService.getAllHotel().subscribe(value => {
      console.log(value);
      this.hotels = value;
    });
  }

  delete(id: number): void {
    this.hotelService.deleteHotel(id).subscribe(value => {
      this.hotels = this.hotels.filter(eachHotels => eachHotels.id !== id);
    });
  }

}
