import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './user-list/user-list.component';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UserEditorComponent} from './user-editor/user-editor.component';
import {UserUpdateComponent} from './user-update/user-update.component';
import {ContinentListComponent} from './continent-list/continent-list.component';
import {ContinentUpdateComponent} from './continent-update/continent-update.component';
import {ContinentEditorComponent} from './continent-editor/continent-editor.component';
import {CountryListComponent} from './country-list/country-list.component';
import {CountryUpdateComponent} from './country-update/country-update.component';
import {CountryEditorComponent} from './country-editor/country-editor.component';
import {CityListComponent} from './city-list/city-list.component';
import {CityEditorComponent} from './city-editor/city-editor.component';
import {CityUpdateComponent} from './city-update/city-update.component';
import {AirportListComponent} from './airport-list/airport-list.component';
import {AirportUpdateComponent} from './airport-update/airport-update.component';
import {AirportEditorComponent} from './airport-editor/airport-editor.component';
import {HotelListComponent} from './hotel-list/hotel-list.component';
import {HotelUpdateComponent} from './hotel-update/hotel-update.component';
import {HotelEditorComponent} from './hotel-editor/hotel-editor.component';
import {TripListComponent} from './trip-list/trip-list.component';
import {TripUpdateComponent} from './trip-update/trip-update.component';
import {TripEditorComponent} from './trip-editor/trip-editor.component';
import {PurchasedTripListComponent} from './purchased-trip-list/purchased-trip-list.component';
import {PurchasedTripUpdateComponent} from './purchased-trip-update/purchased-trip-update.component';
import {PurchasedTripEditorComponent} from './purchased-trip-editor/purchased-trip-editor.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: 'user-list', component: UserListComponent},
  {path: 'user-update', component: UserUpdateComponent},
  {path: 'user-editor', component: UserEditorComponent},
  {path: 'user-editor/:id', component: UserEditorComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'continent-list', component: ContinentListComponent},
  {path: 'continent-update', component: ContinentUpdateComponent},
  {path: 'continent-editor', component: ContinentEditorComponent},
  {path: 'continent-editor/:id', component: ContinentEditorComponent},
  {path: 'country-list', component: CountryListComponent},
  {path: 'country-update', component: CountryUpdateComponent},
  {path: 'country-editor', component: CountryEditorComponent},
  {path: 'country-editor/:id', component: CountryEditorComponent},
  {path: 'city-list', component: CityListComponent},
  {path: 'city-update', component: CityUpdateComponent},
  {path: 'city-editor', component: CityEditorComponent},
  {path: 'city-editor/:id', component: CityEditorComponent},
  {path: 'airport-list', component: AirportListComponent},
  {path: 'airport-update', component: AirportUpdateComponent},
  {path: 'airport-editor', component: AirportEditorComponent},
  {path: 'airport-editor/:id', component: AirportEditorComponent},
  {path: 'hotel-list', component: HotelListComponent},
  {path: 'hotel-update', component: HotelUpdateComponent},
  {path: 'hotel-editor', component: HotelEditorComponent},
  {path: 'hotel-editor/:id', component: HotelEditorComponent},
  {path: 'trip-list', component: TripListComponent},
  {path: 'trip-update', component: TripUpdateComponent},
  {path: 'trip-editor', component: TripEditorComponent},
  {path: 'trip-editor/:id', component: TripEditorComponent},
  {path: 'purchasedTrip-list', component: PurchasedTripListComponent},
  {path: 'purchasedTrip-update', component: PurchasedTripUpdateComponent},
  {path: 'purchasedTrip-editor', component: PurchasedTripEditorComponent},
  {path: 'purchasedTrip-editor/:id', component: PurchasedTripEditorComponent},
  {path: 'login', component: LoginComponent}

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
