import {Component, OnInit} from '@angular/core';
import {City} from '../shared/model/city';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {Country} from '../shared/model/country';
import {CountryService} from '../shared/service/country.service';

@Component({
  selector: 'app-city-editor',
  templateUrl: './city-editor.component.html',
  styleUrls: ['./city-editor.component.css']
})
export class CityEditorComponent implements OnInit {

  city: City = new City();
  country: Country = null;
  countries: Country[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private countryService: CountryService) {
  }

  ngOnInit() {
    this.country = new Country();
    this.countryService.getAllCountries().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.cityService.getCity(id).subscribe(value => {
        console.log(value);
        this.city = value;
        this.city.id = id;
      });
    }
  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.city);
    this.city.id = id;
    this.cityService.updateCity(this.city);
    window.location.href = '/city-list';
  }

  goBack(): void {
    this.location.back();
  }

}
