import {Component, OnInit} from '@angular/core';
import {Trip} from '../shared/model/trip';
import {City} from '../shared/model/city';
import {Airport} from '../shared/model/Airport';
import {Hotel} from '../shared/model/hotel';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {HotelService} from '../shared/service/hotel.service';
import {AirportService} from '../shared/service/airport.service';
import {TripService} from '../shared/service/trip.service';

@Component({
  selector: 'app-trip-update',
  templateUrl: './trip-update.component.html',
  styleUrls: ['./trip-update.component.css']
})
export class TripUpdateComponent implements OnInit {

  trip: Trip = null;
  city: City = null;
  cities: City[];
  airport: Airport = null;
  airports: Airport[];
  hotel: Hotel = null;
  hotels: Hotel[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private hotelService: HotelService,
              private airportService: AirportService,
              private tripService: TripService) {
  }

  ngOnInit() {
    this.trip = new Trip();
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
    this.airport = new Airport();
    this.airportService.getAllAirport().subscribe(value => {
      console.log(value);
      this.airports = value;
    });
    this.hotel = new Hotel();
    this.hotelService.getAllHotel().subscribe(value => {
      console.log(value);
      this.hotels = value;
    });
  }

  save(): void {
    console.log(this.trip);
    this.tripService.saveTrip(this.trip);
    window.location.href = '/trip-list';
  }

  goBack(): void {
    this.location.back();
  }

}
