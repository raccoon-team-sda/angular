import {Component, OnInit} from '@angular/core';
import {TripService} from '../shared/service/trip.service';
import {Trip} from '../shared/model/trip';

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.css']
})
export class TripListComponent implements OnInit {

  constructor(private tripService: TripService) {
  }

  trips: Trip[];

  ngOnInit() {
    this.tripService.getAllTrip().subscribe(value => {
      console.log(value);
      this.trips = value;
    });
  }

  delete(id: number): void {
    this.tripService.deleteTrip(id).subscribe(value => {
      this.trips = this.trips.filter(eachTrips => eachTrips.id !== id);
    });
  }

}
