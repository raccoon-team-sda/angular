import {Component, OnInit} from '@angular/core';
import {PurchasedTrip} from '../shared/model/purchasedTrip';
import {Trip} from '../shared/model/trip';
import {User} from '../shared/model/user';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {PurchasedTripService} from '../shared/service/purchased-trip.service';
import {TripService} from '../shared/service/trip.service';
import {UserServiceService} from '../shared/service/user-service.service';

@Component({
  selector: 'app-purchased-trip-update',
  templateUrl: './purchased-trip-update.component.html',
  styleUrls: ['./purchased-trip-update.component.css']
})
export class PurchasedTripUpdateComponent implements OnInit {

  purchasedTrip: PurchasedTrip = null;
  trip: Trip = null;
  trips: Trip[];
  user: User = null;
  users: User[];


  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private purchasedTripService: PurchasedTripService,
              private tripService: TripService,
              private userService: UserServiceService) {
  }

  ngOnInit() {
    this.purchasedTrip = new PurchasedTrip();
    this.trip = new Trip();
    this.tripService.getAllTrip().subscribe(value => {
      console.log(value)
      this.trips = value;
    });
    this.user = new User();
    this.userService.getAllUsers().subscribe(value => {
      console.log(value);
      this.users = value;
    });
  }

  save(): void {
    console.log(this.purchasedTrip);
    this.purchasedTripService.savePurchasedTrip(this.purchasedTrip);
    window.location.href = '/purchasedTrip-list';
  }

  goBack(): void {
    this.location.back();
  }

}
