import {Component, OnInit} from '@angular/core';
import {Continent} from '../shared/model/continent';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ContinentService} from '../shared/service/continent.service';

@Component({
  selector: 'app-continent-editor',
  templateUrl: './continent-editor.component.html',
  styleUrls: ['./continent-editor.component.css']
})
export class ContinentEditorComponent implements OnInit {

  continent: Continent = new Continent();

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private continentService: ContinentService) {
  }

  ngOnInit() {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.continentService.getContinent(id).subscribe(value => {
        console.log(value);
        this.continent = value;
        this.continent.id = id;
      });
    }
  }
  onUpdate(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.continent);
    this.continent.id = id;
    this.continentService.updateContinent(this.continent);
    window.location.href = '/continent-list';
  }
  goBack(): void {
    // wywołanie poprzedniego url
    this.location.back();
  }

}
