import {Component, OnInit} from '@angular/core';
import {HotelService} from '../shared/service/hotel.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';
import {Hotel} from '../shared/model/hotel';

@Component({
  selector: 'app-hotel-update',
  templateUrl: './hotel-update.component.html',
  styleUrls: ['./hotel-update.component.css']
})
export class HotelUpdateComponent implements OnInit {

  hotel: Hotel = null;
  city: City = null;
  cities: City[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private hotelService: HotelService) {
  }


  ngOnInit() {
    this.hotel = new Hotel();
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
  }

  save(): void {
    console.log(this.hotel);
    this.hotelService.saveHotel(this.hotel);
    window.location.href = '/hotel-list';
  }

  goBack(): void {
    this.location.back();
  }

}
