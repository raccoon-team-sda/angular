import {Component, OnInit} from '@angular/core';
import {CountryService} from '../shared/service/country.service';
import {Country} from '../shared/model/country';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {

  constructor(private countryService: CountryService) {
  }

  countries: Country[];

  ngOnInit() {
    this.countryService.getAllCountries().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
  }

  delete(id: number): void {
    this.countryService.deleteCountry(id).subscribe(value => {
      this.countries = this.countries.filter(eachUsers => eachUsers.id !== id);
    });
  }
}
