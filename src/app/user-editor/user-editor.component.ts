import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../shared/service/user-service.service';
import {User} from '../shared/model/user';

@Component({
  selector: 'app-user-editor',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.css']
})
export class UserEditorComponent implements OnInit {

  user: User = new User();


  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserServiceService) {
  }

  ngOnInit() {
    // pobieranie wartości id z url z przeglądarki
    const idParam = this.route.snapshot.paramMap.get('id');
    // konwersja typu string do int
    const id: number = parseInt(idParam, 10);
    // wyświetlenie id w przeglądrce w trybie develop(F12)
    console.log('id:' + id);
    // jesli id nie jets nullem i nie jest wartością pustą
    if (id) {
      // wywołanie usługi pobierania użytkownika po id
      this.userService.getUser(id).subscribe(value => {
        console.log(value);
        this.user = value;
        this.user.id = id;
      });
    }
  }

  onUpdate(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.user);
    this.user.id = id;
    this.userService.update(this.user);
    window.location.href = '/user-list';
  }

  // metoda wywołana po przyciśnięciu przycisku
  goBack(): void {
    // wywołanie poprzedniego url
    this.location.back();
  }
}
