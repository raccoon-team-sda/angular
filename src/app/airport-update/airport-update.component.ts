import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {AirportService} from '../shared/service/airport.service';
import {Airport} from '../shared/model/Airport';
import {City} from '../shared/model/city';

@Component({
  selector: 'app-airport-update',
  templateUrl: './airport-update.component.html',
  styleUrls: ['./airport-update.component.css']
})
export class AirportUpdateComponent implements OnInit {

  airport: Airport = null;
  city: City = null;
  cities: City[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private airportService: AirportService) {
  }

  ngOnInit() {
    this.airport = new Airport();
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
  }

  save(): void {
    console.log(this.airport);
    this.airportService.saveAirport(this.airport);
    window.location.href = '/airport-list';
  }

  goBack(): void {
    this.location.back();
  }

}
