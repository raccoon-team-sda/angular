import {Component, OnInit} from '@angular/core';
import {Country} from '../shared/model/country';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CountryService} from '../shared/service/country.service';
import {Continent} from '../shared/model/continent';
import {ContinentService} from '../shared/service/continent.service';

@Component({
  selector: 'app-country-update',
  templateUrl: './country-update.component.html',
  styleUrls: ['./country-update.component.css']
})
export class CountryUpdateComponent implements OnInit {

  country: Country = null;
  countries: Country[];
  continent: Continent = null;
  continents: Continent[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private countryService: CountryService,
              private continentService: ContinentService) {
  }

  ngOnInit() {
    this.country = new Country();
    this.continent = new Continent();
    this.continentService.getAllContinents().subscribe(value => {
      console.log(value);
      this.continents = value;
    });
  }

  save(): void {
    console.log(this.country);
    this.countryService.saveCountry(this.country);
    window.location.href = '/country-list';
  }

  goBack(): void {
    this.location.back();
  }

}
