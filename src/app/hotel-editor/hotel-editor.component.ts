import {Component, OnInit} from '@angular/core';
import {Hotel} from '../shared/model/hotel';
import {City} from '../shared/model/city';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {HotelService} from '../shared/service/hotel.service';

@Component({
  selector: 'app-hotel-editor',
  templateUrl: './hotel-editor.component.html',
  styleUrls: ['./hotel-editor.component.css']
})
export class HotelEditorComponent implements OnInit {

  hotel: Hotel = new Hotel();
  city: City = null;
  cities: City[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private hotelService: HotelService) {
  }

  ngOnInit() {
    this.city = new City();
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.hotelService.getHotel(id).subscribe(value => {
        console.log(value);
        this.hotel = value;
        this.hotel.id = id;
      });
    }
  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.hotel);
    this.hotel.id = id;
    this.hotelService.updateHotel(this.hotel);
    window.location.href = '/hotel-list';
  }

  goBack(): void {
    this.location.back();
  }
}
