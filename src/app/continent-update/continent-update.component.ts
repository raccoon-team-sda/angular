import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ContinentService} from '../shared/service/continent.service';
import {Continent} from '../shared/model/continent';

@Component({
  selector: 'app-continent-update',
  templateUrl: './continent-update.component.html',
  styleUrls: ['./continent-update.component.css']
})
export class ContinentUpdateComponent implements OnInit {

  continent: Continent = new Continent();

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private continentService: ContinentService) {
  }

  ngOnInit() {
  }

  onSave(): void {
    console.log(this.continent);
    // zapis użytkownika
    this.continentService.saveContinent(this.continent);
    // wywołanie(z przeładowaniem strony) url w przeglądarce
    window.location.href = '/continent-list';
  }


  goBack(): void {
    this.location.back();
  }
}
