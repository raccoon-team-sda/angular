import {Component, OnInit} from '@angular/core';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {

  constructor(private cityService: CityService) {
  }

  cities: City[];

  ngOnInit() {
    this.cityService.getAllCities().subscribe(value => {
      console.log(value);
      this.cities = value;
    });
  }

  delete(id: number): void {
    this.cityService.deleteCity(id).subscribe(value => {
      this.cities = this.cities.filter(eachCities => eachCities.id !== id);
    });
  }
}
