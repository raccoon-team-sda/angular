import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {TripService} from '../shared/service/trip.service';
import {PurchasedTripService} from '../shared/service/purchased-trip.service';
import {UserServiceService} from '../shared/service/user-service.service';
import {PurchasedTrip} from '../shared/model/purchasedTrip';
import {Trip} from '../shared/model/trip';
import {User} from '../shared/model/user';

@Component({
  selector: 'app-purchased-trip-editor',
  templateUrl: './purchased-trip-editor.component.html',
  styleUrls: ['./purchased-trip-editor.component.css']
})
export class PurchasedTripEditorComponent implements OnInit {

  purchasedTrip: PurchasedTrip = new PurchasedTrip();
  trip: Trip = null;
  trips: Trip[];
  user: User = null;
  users: User[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private purchasedTripService: PurchasedTripService,
              private tripService: TripService,
              private userService: UserServiceService) {
  }

  ngOnInit() {
    this.trip = new Trip();
    this.tripService.getAllTrip().subscribe(value => {
      this.trips = value;
    });
    this.user = new User();
    this.userService.getAllUsers().subscribe(value => {
      console.log(value);
      this.users = value;
    });
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log('id:' + id);
    if (id) {
      this.purchasedTripService.getPurchasedTrip(id).subscribe(value => {
        console.log(value);
        this.purchasedTrip = value;
        this.purchasedTrip.id = id;
      });
    }
  }

  update(): void {
    const idParam = this.route.snapshot.paramMap.get('id');
    const id: number = parseInt(idParam, 10);
    console.log(this.trip);
    this.purchasedTrip.id = id;
    this.purchasedTripService.updatePurchasedTrip(this.purchasedTrip);
    window.location.href = '/purchasedTrip-list';

  }

  goBack(): void {
    this.location.back();
  }

}
