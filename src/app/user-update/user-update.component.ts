import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {User} from '../shared/model/user';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../shared/service/user-service.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  user: User = new User();

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserServiceService) {
  }

  ngOnInit() {
  }

  save(): void {
    console.log(this.user);
    // zapis użytkownika
    this.userService.save(this.user);
    // wywołanie(z przeładowaniem strony) url w przeglądarce
    window.location.href = '/user-list';
  }

  goBack(): void {
    this.location.back();
  }

}
