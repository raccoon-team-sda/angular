import {Component, OnInit} from '@angular/core';
import {PurchasedTripService} from '../shared/service/purchased-trip.service';
import {PurchasedTrip} from '../shared/model/purchasedTrip';

@Component({
  selector: 'app-purchased-trip-list',
  templateUrl: './purchased-trip-list.component.html',
  styleUrls: ['./purchased-trip-list.component.css']
})
export class PurchasedTripListComponent implements OnInit {

  constructor(private purchasedTripService: PurchasedTripService) {
  }

  purchasedTrips: PurchasedTrip[];

  ngOnInit() {
    this.purchasedTripService.getAllPurchasedTrip().subscribe(value => {
      console.log(value);
      this.purchasedTrips = value;
    });
  }

  delete(id: number): void {
    this.purchasedTripService.deletePurchasedTrip(id).subscribe(value => {
      this.purchasedTrips = this.purchasedTrips.filter(eachPurchasedTrips => eachPurchasedTrips.id !== id);
    });
  }

}
