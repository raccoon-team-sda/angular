import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UserListComponent} from './user-list/user-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {UserEditorComponent} from './user-editor/user-editor.component';
import {UserUpdateComponent} from './user-update/user-update.component';
import {ContinentListComponent} from './continent-list/continent-list.component';
import {ContinentUpdateComponent} from './continent-update/continent-update.component';
import {ContinentEditorComponent} from './continent-editor/continent-editor.component';
import {CountryListComponent} from './country-list/country-list.component';
import {CountryUpdateComponent} from './country-update/country-update.component';
import {CountryEditorComponent} from './country-editor/country-editor.component';
import {CityListComponent} from './city-list/city-list.component';
import {CityUpdateComponent} from './city-update/city-update.component';
import {CityEditorComponent} from './city-editor/city-editor.component';
import { AirportListComponent } from './airport-list/airport-list.component';
import { AirportUpdateComponent } from './airport-update/airport-update.component';
import { AirportEditorComponent } from './airport-editor/airport-editor.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { HotelUpdateComponent } from './hotel-update/hotel-update.component';
import { HotelEditorComponent } from './hotel-editor/hotel-editor.component';
import { TripListComponent } from './trip-list/trip-list.component';
import { TripEditorComponent } from './trip-editor/trip-editor.component';
import { TripUpdateComponent } from './trip-update/trip-update.component';
import { PurchasedTripListComponent } from './purchased-trip-list/purchased-trip-list.component';
import { PurchasedTripUpdateComponent } from './purchased-trip-update/purchased-trip-update.component';
import { PurchasedTripEditorComponent } from './purchased-trip-editor/purchased-trip-editor.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    DashboardComponent,
    UserEditorComponent,
    UserUpdateComponent,
    ContinentListComponent,
    ContinentUpdateComponent,
    ContinentEditorComponent,
    CountryListComponent,
    CountryUpdateComponent,
    CountryEditorComponent,
    CityListComponent,
    CityUpdateComponent,
    CityEditorComponent,
    AirportListComponent,
    AirportUpdateComponent,
    AirportEditorComponent,
    HotelListComponent,
    HotelUpdateComponent,
    HotelEditorComponent,
    TripListComponent,
    TripEditorComponent,
    TripUpdateComponent,
    PurchasedTripListComponent,
    PurchasedTripUpdateComponent,
    PurchasedTripEditorComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
