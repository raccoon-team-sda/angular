import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {CityService} from '../shared/service/city.service';
import {City} from '../shared/model/city';
import {isNonDeclarationTsFile} from '@angular/compiler-cli/src/ngtsc/shims/src/util';
import {Country} from '../shared/model/country';
import {CountryService} from '../shared/service/country.service';

@Component({
  selector: 'app-city-update',
  templateUrl: './city-update.component.html',
  styleUrls: ['./city-update.component.css']
})
export class CityUpdateComponent implements OnInit {

  city: City = null;
  country: Country = null;
  countries: Country[];

  constructor(private location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private cityService: CityService,
              private countryService: CountryService) {
  }

  ngOnInit() {
    this.city = new City();
    this.country = new Country();
    this.countryService.getAllCountries().subscribe(value => {
      console.log(value);
      this.countries = value;
    });
  }

  save(): void {
    console.log(this.city);
    this.cityService.saveCity(this.city);
    window.location.href = '/city-list';
  }

  goBack(): void {
    this.location.back();
  }

}
